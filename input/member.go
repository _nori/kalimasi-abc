package input

import (
	"errors"
	"kalimasi/doc"
	"kalimasi/util"
	"regexp"
	"time"

	"github.com/globalsign/mgo/bson"
)

type BasicMember struct {
	Typ         string `json:"mType"`
	IDnumber    string `json:"mid"`
	Name        string
	Sex         int
	Birthday    time.Time
	Email       string
	Address     string
	ServiceUnit string `json:"serviceUnit"`
	Contact     []struct {
		Typ   string `json:"ctType"`
		Value string
	}
}

func (cu *BasicMember) Validate() error {
	if !util.IsStrInList(cu.Typ, doc.TypeMemberPerson, doc.TypeMemberEnterprise) {
		return errors.New("invalid mType")
	}

	if cu.Typ == doc.TypeMemberPerson && !util.IsIdNumber(cu.IDnumber) {
		return errors.New("invalid mid")
	}

	if cu.Typ == doc.TypeMemberEnterprise {
		if m, _ := regexp.MatchString(`^\d{8}$`, cu.IDnumber); !m {
			return errors.New("invalid mid")
		}
	}

	if cu.Sex < doc.SexGirl || cu.Sex > doc.SexNone {
		return errors.New("invalid sex")
	}

	if ok, err := util.IsMail(cu.Email); !ok {
		return err
	}

	for _, c := range cu.Contact {
		if !util.IsStrInList(c.Typ, doc.TypeContactMobile, doc.TypeContactOffice, doc.TypeContactHome) {
			return errors.New("invalid contact type")
		}
	}

	return nil
}

func (cu *BasicMember) GetMemberDoc() *doc.Member {
	m := &doc.Member{
		Typ:         cu.Typ,
		IDnumber:    cu.IDnumber,
		Name:        cu.Name,
		Sex:         cu.Sex,
		Birthday:    cu.Birthday,
		Email:       cu.Email,
		Address:     cu.Address,
		ServiceUnit: cu.ServiceUnit,
	}
	var contact []*doc.Contact
	for _, c := range cu.Contact {
		contact = append(contact, &doc.Contact{
			Typ:   c.Typ,
			Value: c.Value,
		})
	}
	m.Contact = contact
	return m
}

type CreateMember struct {
	BasicMember
	Account struct {
		Date      time.Time `json:"payDate"`
		JoinFee   int       `json:"joinFee"`
		AnnualFee int       `json:"annualFee"`
		Method    string
		Desc      string
	}
}

func (cu *CreateMember) Validate() error {
	if err := cu.BasicMember.Validate(); err != nil {
		return err
	}
	if cu.Account.JoinFee+cu.Account.AnnualFee <= 0 {
		return errors.New("invalid fee")
	}

	if cu.Account.Date.IsZero() {
		return errors.New("invalid payDate")
	}

	return nil
}

type PutMember struct {
	BasicMember
	ID bson.ObjectId
}

func (cu *PutMember) Validate() error {
	if err := cu.BasicMember.Validate(); err != nil {
		return err
	}
	if !cu.ID.Valid() {
		return errors.New("invali id")
	}
	return nil
}

type CreatePayRecord struct {
	Date    time.Time `json:"payDate"`
	PayType string    `json:"payType"`
	Amount  int
	Method  string
	Desc    string
}

func (cu *CreatePayRecord) Validate() error {
	if cu.Amount <= 0 {
		return errors.New("invali amount")
	}
	return nil
}

type QueryMember struct {
	Name      string
	Email     string
	Phone     string
	CompanyID bson.ObjectId
}

func (qb *QueryMember) Validate() error {
	if !qb.CompanyID.Valid() {
		return errors.New("invalid companyID")
	}
	return nil
}

func (qb *QueryMember) GetMgoQuery() bson.M {
	q := bson.M{
		"companyid": qb.CompanyID,
	}
	if qb.Name != "" {
		q["name"] = bson.RegEx{Pattern: qb.Name, Options: "m"}
	}
	if qb.Email != "" {
		q["email"] = bson.RegEx{Pattern: qb.Email, Options: "m"}
	}
	if qb.Phone != "" {
		q["contact"] = bson.M{"$elemMatch": bson.M{"value": bson.RegEx{Pattern: qb.Phone, Options: "m"}}}
	}
	return q
}
