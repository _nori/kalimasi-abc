package input

import (
	"errors"
	"kalimasi/doc"
	"kalimasi/report"
	"strconv"

	"github.com/globalsign/mgo/bson"
)

type QueryBudgetReport struct {
	Current, Last string
}

func (qb *QueryBudgetReport) Validate() error {
	if !bson.IsObjectIdHex(qb.Current) {
		return errors.New("invalid current")
	}
	if qb.Last != "" && !bson.IsObjectIdHex(qb.Last) {
		return errors.New("invalid last")
	}
	return nil
}

func (qb *QueryBudgetReport) GetMgoQuery() bson.M {
	if qb.Last == "" {
		return bson.M{
			"_id": bson.ObjectIdHex(qb.Current),
		}
	}
	return bson.M{
		"_id": bson.M{"$in": []bson.ObjectId{
			bson.ObjectIdHex(qb.Current),
			bson.ObjectIdHex(qb.Last)}},
	}
}

type QueryFinalAccountReport struct {
	Budget string
}

func (qb *QueryFinalAccountReport) Validate() error {
	if !bson.IsObjectIdHex(qb.Budget) {
		return errors.New("invalid budget")
	}
	return nil
}

func (qb *QueryFinalAccountReport) GetMgoQuery() bson.M {
	return bson.M{
		"_id": bson.ObjectIdHex(qb.Budget),
	}
}

type QueryTaxCalReport struct {
	Year string
	year int
}

func (qb *QueryTaxCalReport) GetYear() int {
	if qb.year > 0 {
		return qb.year
	}
	y, _ := strconv.Atoi(qb.Year)
	qb.year = y
	return qb.year
}

func (qb *QueryTaxCalReport) Validate() error {
	y, err := strconv.Atoi(qb.Year)
	if err != nil {
		return err
	}
	qb.year = y
	return nil
}

func (qb *QueryTaxCalReport) GetMgoQuery() bson.M {
	return bson.M{
		"year": qb.year,
	}
}

type CreateTaxFile struct {
	report.TaxCal
}

func (qb *CreateTaxFile) Validate() error {
	if qb.Year <= 0 {
		return errors.New("invalid year")
	}
	if len(qb.Incomes) == 0 {
		return errors.New("missing incomes")
	}
	if len(qb.Expenditure) == 0 {
		return errors.New("missing expenditure")
	}
	if qb.StartTime.IsZero() {
		return errors.New("missing startTime")
	}
	if qb.EndTime.IsZero() {
		return errors.New("missing endTime")
	}
	return nil
}

func (bi *CreateTaxFile) GetDoc(companyID bson.ObjectId) *doc.TaxFile {
	r := &doc.TaxFile{
		Year:              bi.Year,
		StartDate:         bi.StartTime,
		EndDate:           bi.EndTime,
		CompanyID:         companyID,
		Income:            bi.Incomes,
		Expenses:          bi.Expenditure,
		IsTaxFree:         bi.IsTaxFree,
		TaxFreeAmount:     bi.TaxFreeAmount,
		OpenMonth:         bi.OpenMonth,
		TaxCredit:         bi.TaxCredit,
		OverseasTaxCredit: bi.OverseasTaxCredit,
		LostInvoice:       bi.LostInvoice,
	}
	return r
}
