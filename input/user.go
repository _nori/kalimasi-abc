package input

import (
	"context"
	"errors"
	"net/http"

	"github.com/asaskevich/govalidator"

	"github.com/globalsign/mgo/bson"
)

type ReqUser struct {
	ID        string
	Name      string
	Account   string
	CompanyID bson.ObjectId
	Perm      string
}

const ctxKeyReqUser = "userInfo"

func (ru *ReqUser) GetName() string {
	return ru.Name
}

func (ru *ReqUser) GetAcc() string {
	return ru.Account
}

func (ru *ReqUser) GetCompany() bson.ObjectId {
	return ru.CompanyID
}

func (ru *ReqUser) SaveToContext(r *http.Request) *http.Request {
	ctx := context.WithValue(r.Context(), ctxKeyReqUser, ru)
	return r.WithContext(ctx)
}

func GetUserInfo(req *http.Request) *ReqUser {
	ctx := req.Context()
	reqID := ctx.Value(ctxKeyReqUser)

	if ret, ok := reqID.(*ReqUser); ok {
		return ret
	}
	return nil
}

type Login struct {
	Account       string
	Pwd           string
	Company       string
	IsFakeCompany bool
}

func (qb *Login) Validate() error {
	if !govalidator.IsEmail(qb.Account) {
		return errors.New("invalid account")
	}
	if qb.Pwd == "" {
		return errors.New("miss password")
	}
	if len(qb.Company) != 8 {
		return errors.New("invalid company")
	}
	return nil
}

type LoginV2 struct {
	Account string
	Pwd     string
}

func (qb *LoginV2) Validate() error {
	if !govalidator.IsEmail(qb.Account) {
		return errors.New("invalid account")
	}
	if qb.Pwd == "" {
		return errors.New("miss password")
	}
	return nil
}
