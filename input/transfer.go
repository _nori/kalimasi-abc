package input

import (
	"errors"
	"strconv"
	"time"

	"kalimasi/doc"

	"github.com/asaskevich/govalidator"
	"github.com/globalsign/mgo/bson"
)

type CreateTransfer struct {
	BudgetId *bson.ObjectId `json:"budgetId"`
	Date     time.Time      `json:"tDate"`
	Summary  string

	DebitAccTerm  []InputAccTerm `json:"debitAccTerm"`
	CreditAccTerm []InputAccTerm `json:"creditAccTerm"`
}

func (bi *CreateTransfer) GetTransferDoc() *doc.Transfer {
	r := &doc.Transfer{
		DateTime: bi.Date,
		Year:     bi.Date.Year() - 1911,
		BudgetId: bi.BudgetId,
		Summary:  bi.Summary,
	}
	return r
}

func (cu *CreateTransfer) Validate() error {
	if len(cu.CreditAccTerm) == 0 || len(cu.DebitAccTerm) == 0 {
		return errors.New("must have credit account and debit account")
	}
	creditTotal, debitTotal := 0, 0
	for _, c := range cu.CreditAccTerm {
		if err := c.Validate(); err != nil {
			return err
		}
		creditTotal += c.Amount
	}
	for _, d := range cu.DebitAccTerm {
		if err := d.Validate(); err != nil {
			return err
		}
		debitTotal += d.Amount
	}
	if debitTotal != creditTotal {
		return errors.New("not balance")
	}
	return nil
}

type PutTransfer struct {
	CreateTransfer
	ID            bson.ObjectId
	DeleteAccterm []bson.ObjectId `json:"deleteAccterm"`
}

func (pb *PutTransfer) Validate() error {
	err := pb.CreateTransfer.Validate()
	if err != nil {
		return err
	}
	if !pb.ID.Valid() {
		return errors.New("invalid receipt id")
	}
	return nil
}

type QueryTransfer struct {
	Summary   string
	Amount    string
	CompanyID bson.ObjectId

	BudgetId  string
	StartDate string
}

func (qb *QueryTransfer) Validate() error {
	if !qb.CompanyID.Valid() {
		return errors.New("invalid companyID")
	}
	if !govalidator.IsNumeric(qb.Amount) {
		return errors.New("invalid amount")
	}
	return nil
}

func (qb *QueryTransfer) GetMgoQuery() bson.M {
	q := bson.M{
		"companyid": qb.CompanyID,
	}
	if qb.Amount != "" {
		i, err := strconv.Atoi(qb.Amount)
		if err == nil {
			q["amount"] = i
		}
	}
	if qb.Summary != "" {
		q["summary"] = bson.RegEx{Pattern: qb.Summary, Options: "m"}
	}
	if qb.BudgetId != "" {
		id, err := doc.GetObjectID(qb.BudgetId)
		if err == nil {
			q["budgetid"] = id
		}
	}
	if qb.StartDate != "" {
		t, err := time.Parse(time.RFC3339, qb.StartDate)
		if err == nil {
			q["datetime"] = t
		}

	}
	return q
}
