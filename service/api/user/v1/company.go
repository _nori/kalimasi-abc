package v1

import (
	"encoding/json"
	"kalimasi/doc"
	"kalimasi/input"
	inputreport "kalimasi/input/reportgener"
	"kalimasi/model"
	"kalimasi/model/reportgener"
	"kalimasi/rsrc"
	"kalimasi/util"
	"net/http"
)

type CompanyAPI string

func (api CompanyAPI) GetName() string {
	return string(api)
}

func (a CompanyAPI) GetAPIs() []*rsrc.APIHandler {
	return []*rsrc.APIHandler{
		&rsrc.APIHandler{Path: "/v1/company/conf", Next: a.getConfEndpoint, Method: "GET", Auth: true},

		&rsrc.APIHandler{Path: "/v1/company/pay", Next: a.createPayMethodEndpoint, Method: "POST", Auth: true},
		&rsrc.APIHandler{Path: "/v1/company/{ID}/pay", Next: a.modifyPayMethodEndpoint, Method: "PUT", Auth: true},
		&rsrc.APIHandler{Path: "/v1/company/{ID}/pay/{NAME}", Next: a.deletePayMethodEndpoint, Method: "DELETE", Auth: true},

		&rsrc.APIHandler{Path: "/v1/company/income", Next: a.createIncomeEndpoint, Method: "POST", Auth: true},
		&rsrc.APIHandler{Path: "/v1/company/{ID}/income", Next: a.modifyIncomeEndpoint, Method: "PUT", Auth: true},
		&rsrc.APIHandler{Path: "/v1/company/{ID}/income/{NAME}", Next: a.deleteIncomeEndpoint, Method: "DELETE", Auth: true},

		&rsrc.APIHandler{Path: "/v1/company/budget", Next: a.getBudgetSettingEndpoint, Method: "GET", Auth: true},
		&rsrc.APIHandler{Path: "/v1/company/budget/{ID}", Next: a.modifyBudgetSettingEndpoint, Method: "PUT", Auth: true},

		&rsrc.APIHandler{Path: "/v1/company/balanceSheet", Next: a.getBalanceSheetSettingEndpoint, Method: "GET", Auth: true},
		&rsrc.APIHandler{Path: "/v1/company/balanceSheet/{ID}", Next: a.modifyBalanceSheetSettingEndpoint, Method: "PUT", Auth: true},
	}
}

func (a CompanyAPI) Init() {

}

func (api *CompanyAPI) getConfEndpoint(w http.ResponseWriter, req *http.Request) {
	qv := util.GetQueryValue(req, []string{"usage"}, true)
	isEdit := (qv["usage"].(string) == "edit")
	ui := input.GetUserInfo(req)

	c := &doc.Company{ID: ui.CompanyID}
	mgoDB := model.GetMgoDBModelByReq(req)
	err := mgoDB.FindByID(c)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(err.Error()))
		return
	}

	payMethod, income := []map[string]interface{}{}, []map[string]interface{}{}
	for _, p := range c.PayMethod {
		if isEdit || p.Enable {
			payMethod = append(payMethod, map[string]interface{}{
				"name":        p.Name,
				"display":     p.Display,
				"enable":      p.Enable,
				"accTermCode": p.AccTermCode,
			})
		}
	}

	for _, i := range c.Incomes {
		if isEdit || i.Enable {
			income = append(income, map[string]interface{}{
				"name":        i.Name,
				"display":     i.Display,
				"enable":      i.Enable,
				"accTermCode": i.AccTermCode,
			})
		}
	}
	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(map[string]interface{}{
		"payMethod": payMethod,
		"incomes":   income,
		"taxRate":   c.TaxRate,
	})
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *CompanyAPI) createPayMethodEndpoint(w http.ResponseWriter, req *http.Request) {
	ca := &input.PayAccTermMapConf{}
	err := json.NewDecoder(req.Body).Decode(ca)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	if err = ca.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	ui := input.GetUserInfo(req)
	dbclt := rsrc.GetDI().GetMongoByReq(req)
	cm := model.GetCompanyModel(dbclt)
	err = cm.CreatePayMethod(ca, ui)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))

}

func (api *CompanyAPI) createIncomeEndpoint(w http.ResponseWriter, req *http.Request) {
	ca := &input.PayAccTermMapConf{}
	err := json.NewDecoder(req.Body).Decode(ca)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	if err = ca.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	ui := input.GetUserInfo(req)
	dbclt := rsrc.GetDI().GetMongoByReq(req)
	cm := model.GetCompanyModel(dbclt)
	err = cm.CreateIncome(ca, ui)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))

}

func (api *CompanyAPI) modifyPayMethodEndpoint(w http.ResponseWriter, req *http.Request) {
	ca := &input.PutPayAccTermMapConf{}
	err := json.NewDecoder(req.Body).Decode(ca)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]
	qid, err := doc.GetObjectID(queryID)

	if err != nil || qid != ca.ID {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("invalid id"))
		return
	}

	if err = ca.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	ui := input.GetUserInfo(req)
	dbclt := rsrc.GetDI().GetMongoByReq(req)
	cm := model.GetCompanyModel(dbclt)
	err = cm.ModifyPayMethod(ca, ui)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))

}

func (api *CompanyAPI) deletePayMethodEndpoint(w http.ResponseWriter, req *http.Request) {

	vars := util.GetPathVars(req, []string{"ID", "NAME"})
	queryID := vars["ID"]
	NAME := vars["NAME"].(string)
	qid, err := doc.GetObjectID(queryID)
	ui := input.GetUserInfo(req)

	if err != nil || qid != ui.CompanyID {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("invalid id"))
		return
	}

	dbclt := rsrc.GetDI().GetMongoByReq(req)
	cm := model.GetCompanyModel(dbclt)
	err = cm.DeletePayMethod(NAME, ui)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))

}

func (api *CompanyAPI) modifyIncomeEndpoint(w http.ResponseWriter, req *http.Request) {
	ca := &input.PutPayAccTermMapConf{}
	err := json.NewDecoder(req.Body).Decode(ca)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]
	qid, err := doc.GetObjectID(queryID)

	if err != nil || qid != ca.ID {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("invalid id"))
		return
	}

	if err = ca.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	ui := input.GetUserInfo(req)
	dbclt := rsrc.GetDI().GetMongoByReq(req)
	cm := model.GetCompanyModel(dbclt)
	err = cm.ModifyIncome(ca, ui)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))

}

func (api *CompanyAPI) deleteIncomeEndpoint(w http.ResponseWriter, req *http.Request) {

	vars := util.GetPathVars(req, []string{"ID", "NAME"})
	queryID := vars["ID"]
	NAME := vars["NAME"].(string)
	qid, err := doc.GetObjectID(queryID)
	ui := input.GetUserInfo(req)

	if err != nil || qid != ui.CompanyID {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("invalid id"))
		return
	}

	dbclt := rsrc.GetDI().GetMongoByReq(req)
	cm := model.GetCompanyModel(dbclt)
	err = cm.DeleteIncome(NAME, ui)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))

}

func (api *CompanyAPI) getBudgetSettingEndpoint(w http.ResponseWriter, req *http.Request) {
	ui := input.GetUserInfo(req)

	c := &doc.Report{ID: doc.GetReportID(doc.BalanceBudget, ui.CompanyID)}
	mgoDB := model.GetMgoDBModelByReq(req)
	err := mgoDB.FindByID(c)
	if err != nil {
		dbclt := rsrc.GetDI().GetMongoByReq(req)
		rm := model.GetReportModel(dbclt)
		r := &reportgener.BalanceBudget{CompanyID: ui.CompanyID}
		rm.SaveSetting(r, ui)

		err := mgoDB.FindByID(c)
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			w.Write([]byte(err.Error()))
			return
		}
	}

	sbb := []*reportgener.SettingBalanceBudget{}
	err = json.Unmarshal([]byte(c.JsonSetting), &sbb)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	if sbb != nil {
		for _, p1 := range sbb {
			p1.SetEmpty()
		}
	} else {
		sbb = []*reportgener.SettingBalanceBudget{}
	}

	w.Header().Set("Content-Type", "application/json")

	err = json.NewEncoder(w).Encode(map[string]interface{}{
		"id":                   c.GetID(),
		"settingBalanceBudget": sbb,
	})

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *CompanyAPI) modifyBudgetSettingEndpoint(w http.ResponseWriter, req *http.Request) {
	ca := &inputreport.PutSettingBalanceBudget{}
	err := json.NewDecoder(req.Body).Decode(ca)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	if err = ca.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]
	qid, err := doc.GetObjectID(queryID)

	ui := input.GetUserInfo(req)
	rid := doc.GetReportID(doc.BalanceBudget, ui.CompanyID)

	if err != nil || qid != ca.ID || rid != ca.ID {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("invalid id"))
		return
	}

	dbclt := rsrc.GetDI().GetMongoByReq(req)
	rm := model.GetReportModel(dbclt)
	err = rm.ModifyReportBalanceBudget(ca, ui)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))

}

func (api *CompanyAPI) getBalanceSheetSettingEndpoint(w http.ResponseWriter, req *http.Request) {
	ui := input.GetUserInfo(req)
	c := &doc.Report{ID: doc.GetReportID(doc.BalanceSheet, ui.CompanyID)}
	mgoDB := model.GetMgoDBModelByReq(req)
	err := mgoDB.FindByID(c)
	if err != nil {

		dbclt := rsrc.GetDI().GetMongoByReq(req)
		rm := model.GetReportModel(dbclt)
		r := &reportgener.BalanceSheet{CompanyID: ui.CompanyID}
		rm.SaveSetting(r, ui)

		err := mgoDB.FindByID(c)
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			w.Write([]byte(err.Error()))
			return
		}
	}

	sbb := reportgener.BalanceSheet{}
	err = json.Unmarshal([]byte(c.JsonSetting), &sbb)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	sbb.SetEmpty()

	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(map[string]interface{}{
		"id":                  c.GetID(),
		"settingBalanceSheet": sbb,
	})

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *CompanyAPI) modifyBalanceSheetSettingEndpoint(w http.ResponseWriter, req *http.Request) {
	ca := &inputreport.PutSettingBalanceSheet{}
	err := json.NewDecoder(req.Body).Decode(ca)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	if err = ca.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]
	qid, err := doc.GetObjectID(queryID)

	ui := input.GetUserInfo(req)
	rid := doc.GetReportID(doc.BalanceSheet, ui.CompanyID)

	if err != nil || qid != ca.ID || rid != ca.ID {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("invalid id"))
		return
	}

	dbclt := rsrc.GetDI().GetMongoByReq(req)
	rm := model.GetReportModel(dbclt)
	err = rm.ModifyReportBalanceSheet(ca, ui)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))

}
