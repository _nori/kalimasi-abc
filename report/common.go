package report

import (
	"errors"
	"io"
)

type commonReport struct {
}

func (c *commonReport) TXT(w io.Writer) error {
	return errors.New("not support txt type")
}

func (c *commonReport) PDF(w io.Writer) error {
	return errors.New("not support pdf type")
}

func (c *commonReport) Json(w io.Writer) error {
	return errors.New("not support json type")
}
