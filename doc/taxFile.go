package doc

import (
	"encoding/binary"
	"kalimasi/util"
	"time"

	"github.com/globalsign/mgo/bson"
	"github.com/globalsign/mgo/txn"
)

type TaxItem struct {
	Term         string `json:"name"`
	Desc         string `json:"desc"`
	Amount       int    `json:"amount"`
	AdjustAmount int    `json:"adjustAmount"`
	Ps           string `json:"ps"`
}

type TaxFile struct {
	ID        bson.ObjectId `bson:"_id"`
	Year      int
	StartDate time.Time
	EndDate   time.Time

	CompanyID bson.ObjectId

	Income   []*TaxItem
	Expenses []*TaxItem

	IsTaxFree         bool // 是否免稅
	TaxFreeAmount     int  // 免稅額
	OpenMonth         int  // 營業月數
	TaxCredit         int  // 抵稅額
	OverseasTaxCredit int  // 境外抵稅額
	LostInvoice       int  // 遺失憑證金額

	CompanyObj *Company `bson:"lookupBudget,omitempty"`

	CommonDoc `bson:"meta"`
}

const (
	taxFileC = "taxFile"
)

func (d *TaxFile) newID() bson.ObjectId {
	var b [12]byte
	if !d.CompanyID.Valid() {
		panic("companyid is invalid")
	}
	orgByte := []byte(string(d.CompanyID)[0:8])
	for i := 0; i < 8; i++ {
		b[i] = orgByte[i]
	}

	bs := make([]byte, 2)
	binary.BigEndian.PutUint16(bs, uint16(d.Year))
	for i := 0; i < 2; i++ {
		b[8+i] = bs[i]
	}
	return bson.ObjectId(b[:])
}

func (u *TaxFile) GetC() string {
	if !u.CompanyID.Valid() {
		panic("must set companyID")
	}
	return util.StrAppend(u.CompanyID.Hex(), taxFileC)
}

func (u *TaxFile) GetID() bson.ObjectId {
	if !u.ID.Valid() {
		u.ID = u.newID()
	}
	return u.ID
}

func (u *TaxFile) GetDoc() interface{} {
	if !u.ID.Valid() {
		u.ID = u.newID()
	}
	return u
}

func (u *TaxFile) GetUpdateField() bson.M {
	return bson.M{
		"startdate":         u.StartDate,
		"enddate":           u.EndDate,
		"income":            u.Income,
		"expenses":          u.Expenses,
		"istaxfree":         u.IsTaxFree,
		"taxfreeamount":     u.TaxFreeAmount,
		"openmonth":         u.OpenMonth,
		"taxcredit":         u.TaxCredit,
		"overseastaxcredit": u.OverseasTaxCredit,
		"lostinvoice":       u.LostInvoice,
	}
}

func (u *TaxFile) GetSaveTxnOp(lu LogUser) txn.Op {
	return u.CommonDoc.getSaveTxnOp(u, lu)
}

func (u *TaxFile) GetUpdateTxnOp(data bson.D) txn.Op {
	return u.CommonDoc.getUpdateTxnOp(u, data)
}

func (u *TaxFile) GetDelTxnOp() txn.Op {
	return u.CommonDoc.getDelTxnOp(u)
}
