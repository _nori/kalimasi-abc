package db

import (
	"errors"
	"fmt"
	"reflect"
	"strings"
	"sync"
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

const (
	normalTimeout = time.Second * 60
	TxnC          = "txn"
)

type MongoV2 struct {
	Host     string `yaml:"host"`
	User     string `yaml:"user"`
	Pass     string `yaml:"pass"`
	Database string `yaml:"database"`

	lock     sync.Mutex
	connPool map[string]*mgo.Session

	mgosession *mgo.Session
}

func (m *MongoV2) GetDB(key string) *mgo.Database {
	if m.connPool == nil {
		m.connPool = make(map[string]*mgo.Session)
	}
	m.lock.Lock()
	defer m.lock.Unlock()
	c, ok := m.connPool[key]
	if ok {
		return c.DB(m.Database)
	}
	c = m.connect()
	m.connPool[key] = c
	return c.DB(m.Database)
}

func (m *MongoV2) Close(key string) {
	m.lock.Lock()
	defer m.lock.Unlock()
	c, ok := m.connPool[key]
	if ok {
		c.Close()
		delete(m.connPool, key)
	}
}

func (m *MongoV2) String() string {
	return fmt.Sprint("mongodb connection pool is ", len(m.connPool))
}

func (m *MongoV2) connect() *mgo.Session {
	if m.mgosession != nil {
		if err := m.mgosession.Ping(); err != nil {
			panic(err)
		}
		return m.mgosession.Copy()
	}
	var address []string
	address = strings.SplitN(m.Host, ",", -1)
	dialInfo := mgo.DialInfo{
		Addrs:         address,
		Timeout:       60 * time.Second,
		ReadTimeout:   5 * time.Second,
		WriteTimeout:  5 * time.Second,
		MaxIdleTimeMS: 2000,
		MinPoolSize:   1000,
	}
	if m.User != "" && m.Pass != "" {
		dialInfo.Username = m.User
		dialInfo.Password = m.Pass
	}
	var err error

	m.mgosession, err = mgo.DialWithInfo(&dialInfo)
	if err != nil {
		panic(err)
	}
	// session.SetPoolLimit(500)
	// session.SetSocketTimeout(normalTimeout)
	// session.SetMode(mgo.Monotonic, true)
	// 註解掉debug訊息
	// mgo.SetDebug(true)
	// var aLogger *log.Logger
	// aLogger = log.New(os.Stderr, "", log.LstdFlags)
	// mgo.SetLogger(aLogger)
	return m.mgosession.Copy()
}

func (m *MongoV2) Ping() error {
	session := m.connect()
	defer session.Close()
	return session.Ping()
}

func GetObjectID(id interface{}) (*bson.ObjectId, error) {
	var myID bson.ObjectId
	switch dtype := reflect.TypeOf(id).String(); dtype {
	case "string":
		str := id.(string)
		if str == "" || !bson.IsObjectIdHex(str) {
			return nil, errors.New("id is error: " + str)
		}
		myID = bson.ObjectIdHex(str)
	case "bson.ObjectId":
		myID = id.(bson.ObjectId)
	default:
		return nil, errors.New("not support type: " + dtype)
	}
	return &myID, nil
}
