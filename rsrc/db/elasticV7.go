package db

import (
	elastic "github.com/olivere/elastic/v7"
)

type Searchv7 struct {
	Hosts []string `yaml:"host"`

	// lock           sync.Mutex
	client *elastic.Client
	//connPool       map[string]*elastic.Client
}

func (s *Searchv7) GetClient() *elastic.Client {
	if s.client != nil {
		return s.client
	}

	c := s.connect()
	s.client = c

	return c
}

func (s *Searchv7) connect() *elastic.Client {
	myclient, err := elastic.NewClient(elastic.SetURL(s.Hosts...), elastic.SetSniff(false))
	if err != nil {
		panic(err)
	}
	return myclient
}

func (s *Searchv7) Close(key string) {
	// s.lock.Lock()
	// defer s.lock.Unlock()
	//delete(s.connPool, key)
}
