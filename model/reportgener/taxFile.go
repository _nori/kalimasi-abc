package reportgener

import (
	"kalimasi/doc"
	"kalimasi/report"
	"kalimasi/rsrc"
	"kalimasi/util"
	"strconv"
	"time"

	"github.com/globalsign/mgo/bson"
)

const (
	taxFileTitle = "title"
	taxFileType  = "taxFile"
)

type TaxFileDocTrans struct {
	Doc *doc.TaxFile
}

func (bbr *TaxFileDocTrans) GetReportObj(title string, font map[string]string) (ReportInter, error) {
	di := rsrc.GetDI()
	style := report.TaxCalStyle(true)
	result := report.TaxCal{
		StartTime:         bbr.Doc.StartDate,
		EndTime:           bbr.Doc.EndDate,
		OrgName:           bbr.Doc.CompanyObj.Name,
		UnitCode:          bbr.Doc.CompanyObj.UnitCode,
		Year:              bbr.Doc.Year,
		Incomes:           bbr.Doc.Income,
		Expenditure:       bbr.Doc.Expenses,
		IsTaxFree:         bbr.Doc.IsTaxFree,
		TaxFreeAmount:     bbr.Doc.TaxFreeAmount,
		OpenMonth:         bbr.Doc.OpenMonth,
		TaxCredit:         bbr.Doc.TaxCredit,
		OverseasTaxCredit: bbr.Doc.OverseasTaxCredit,
		LostInvoice:       bbr.Doc.LostInvoice,

		Style: &style,
		Font:  di.GetFontMap(),
	}
	return &result, nil
}

func (bbr *TaxFileDocTrans) IsSetting() bool {
	return bbr.Doc.Income != nil && bbr.Doc.Expenses != nil
}

func (bbr *TaxFileDocTrans) Setting(setting string) error {
	return nil
}

func (bbr *TaxFileDocTrans) GetData() string {
	panic("not support")
}

func (bbr *TaxFileDocTrans) GetFileName() string {
	return util.StrAppend(bbr.Doc.CompanyObj.ID.Hex(), "/", strconv.Itoa(bbr.Doc.Year), "-taxFile-", strconv.FormatInt(time.Now().Unix(), 16))
}

func (bbr *TaxFileDocTrans) GetType() string {
	return taxFileType
}

func (bbr *TaxFileDocTrans) GetTitle() string {
	return taxFileTitle
}

func (bbr *TaxFileDocTrans) GetReportID() bson.ObjectId {
	if !bbr.Doc.CompanyObj.ID.Valid() {
		panic("must set companyID")
	}
	return doc.GetReportID(bbr.GetType(), bbr.Doc.CompanyObj.ID)
}

type TaxFileTrans struct {
	Year      int
	StartDate time.Time
	EndDate   time.Time

	Company *doc.Company

	Income     []*doc.AccTerm
	Expenses   []*doc.AccTerm
	DataAccess balanceBudgetDataInter
}

func (bbr *TaxFileTrans) IsSetting() bool {
	return bbr.Income != nil && bbr.Expenses != nil && bbr.DataAccess != nil
}

func (bbr *TaxFileTrans) Setting(setting string) error {
	return nil
}

func (bbr *TaxFileTrans) GetData() string {
	panic("not support")
}

func (bbr *TaxFileTrans) GetFileName() string {
	return util.StrAppend(bbr.Company.ID.Hex(), "/", strconv.Itoa(bbr.Year), "-taxFile-", strconv.FormatInt(time.Now().Unix(), 16))
}

func (bbr *TaxFileTrans) GetType() string {
	return taxFileType
}

func (bbr *TaxFileTrans) GetTitle() string {
	return taxFileTitle
}

func (bbr *TaxFileTrans) GetReportID() bson.ObjectId {
	if !bbr.Company.ID.Valid() {
		panic("must set companyID")
	}
	return doc.GetReportID(bbr.GetType(), bbr.Company.ID)
}

func (bbr *TaxFileTrans) GetReportObj(title string, font map[string]string) (ReportInter, error) {
	di := rsrc.GetDI()
	year := bbr.Year + 1911
	start := time.Date(year, time.Month(1), 1, 0, 0, 0, 0, di.Location)
	end := time.Date(year, time.Month(12), 31, 0, 0, 0, 0, di.Location)
	style := report.TaxCalStyle(true)
	result := report.TaxCal{
		StartTime: start,
		EndTime:   end,
		OrgName:   bbr.Company.Name,
		UnitCode:  bbr.Company.UnitCode,
		Year:      bbr.Year,
		Style:     &style,
		Font:      di.GetFontMap(),
	}
	var bi *doc.BudgetItem
	for _, v := range bbr.Income {
		bi = bbr.DataAccess.GetBudgetItem(v.Code, doc.TypeAccIncome)
		if bi == nil {
			continue
		}
		result.AddIncome(&doc.TaxItem{
			Term:         v.Name,
			Amount:       bi.Amount,
			AdjustAmount: bi.Amount,
		})
	}

	for _, v := range bbr.Expenses {
		bi = bbr.DataAccess.GetBudgetItem(v.Code, doc.TypeAccExpand)
		if bi == nil {
			continue
		}
		result.AddExpenditure(&doc.TaxItem{
			Term:         v.Name,
			Amount:       bi.Amount,
			AdjustAmount: bi.Amount,
		})
	}

	return &result, nil
}
