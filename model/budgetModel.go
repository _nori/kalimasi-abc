package model

import (
	"errors"
	"kalimasi/doc"
	"kalimasi/input"
	"kalimasi/rsrc/log"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/globalsign/mgo/txn"
)

type budgetModel struct {
	dbmodel *mgoDBModel
	log     *log.Logger
}

func GetBudgetModel(mgodb *mgo.Database) *budgetModel {
	mongo := GetMgoDBModel(mgodb)
	return &budgetModel{dbmodel: mongo, log: mongo.log}
}

func (bm *budgetModel) Modify(pi *input.PutBudget, u *input.ReqUser) error {
	bs := &doc.BudgetStatement{ID: pi.ID, CompanyID: u.GetCompany()}
	err := bm.dbmodel.FindByID(bs)
	if err != nil {
		return err
	}
	var parentid *bson.ObjectId
	if bson.IsObjectIdHex(pi.ParentID) {
		oid := bson.ObjectIdHex(pi.ParentID)
		parentid = &oid
	} else {
		parentid = nil
	}
	var ops []txn.Op
	ops = append(ops, bs.GetUpdateTxnOp(
		bson.D{
			{Name: "title", Value: pi.Title},
			{Name: "year", Value: pi.Year},
			{Name: "typ", Value: pi.Typ},
			{Name: "income", Value: pi.Income},
			{Name: "expenses", Value: pi.Expenses},
			{Name: "projectcount", Value: len(pi.Project)},
			{Name: "parentid", Value: parentid},
			{Name: "amount", Value: pi.Amount},
			{Name: "startdate", Value: pi.StartDate},
			{Name: "enddate", Value: pi.EndDate},
		}))
	if len(pi.Project) > 0 {
		for _, s := range pi.Project {
			if !bson.IsObjectIdHex(s) {
				continue
			}
			ps := &doc.BudgetStatement{
				ID:        bson.ObjectIdHex(s),
				CompanyID: u.CompanyID,
			}
			ops = append(ops, ps.GetUpdateTxnOp(bson.D{{Name: "parentid", Value: bs.GetID()}}))
		}
	}
	if len(pi.DelProject) > 0 {
		for _, s := range pi.DelProject {
			if !bson.IsObjectIdHex(s) {
				continue
			}
			ps := &doc.BudgetStatement{
				ID:        bson.ObjectIdHex(s),
				CompanyID: u.CompanyID,
			}
			ops = append(ops, ps.GetUpdateTxnOp(bson.D{{Name: "parentid", Value: nil}}))
		}
	}
	err = bm.dbmodel.RunTxn(ops)
	if err != nil {
		return err
	}
	return bm.dbmodel.addDocLog(bs, u, doc.ActUpdate)
}

func (bm *budgetModel) Create(bi *input.CreateBudget, u *input.ReqUser) error {
	bs := bi.ToDoc()
	bs.CompanyID = u.CompanyID
	// 若沒關連的專案，則單儲存年度預算
	if len(bi.Project) == 0 {
		return bm.dbmodel.Save(bs, u)
	}
	// 有關連的專案
	// 儲存年度預算並更新所有現有Project的ParentID
	var ops []txn.Op
	ops = append(ops, bs.GetSaveTxnOp(u))
	for _, s := range bi.Project {
		ps := &doc.BudgetStatement{
			ID:        bson.ObjectIdHex(s),
			CompanyID: u.CompanyID,
		}
		ops = append(ops, ps.GetUpdateTxnOp(bson.D{{Name: "parentid", Value: bs.GetID()}}))
	}
	return bm.dbmodel.RunTxn(ops)
}

func (bm *budgetModel) Delete(pi doc.DocInter, u *input.ReqUser) error {
	or := &doc.BudgetStatement{ID: pi.GetID(), CompanyID: u.CompanyID}
	err := bm.dbmodel.FindByID(or)
	if err != nil {
		return err
	}
	p := &doc.BudgetStatement{CompanyID: u.CompanyID}
	err = bm.dbmodel.FindOne(p, bson.M{"parentid": or.ID})
	if err == nil && p.ID.Valid() {
		return errors.New("has project")
	}
	err = bm.dbmodel.RemoveByID(or)
	if err != nil {
		return err
	}
	return bm.dbmodel.addDocLog(or, u, doc.ActDelete)
}
