module kalimasi

go 1.12

require (
	cloud.google.com/go/firestore v1.2.0 // indirect
	cloud.google.com/go/storage v1.10.0
	firebase.google.com/go v3.13.0+incompatible
	github.com/94peter/gopdf v0.8.1
	github.com/asaskevich/govalidator v0.0.0-20200428143746-21a406dcc535
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/globalsign/mgo v0.0.0-20181015135952-eeefdecb41b8
	github.com/google/uuid v1.1.1
	github.com/gorilla/mux v1.7.4
	github.com/joho/godotenv v1.3.0
	github.com/leekchan/accounting v0.0.0-20191218023648-17a4ce5f94d4
	github.com/olivere/elastic/v7 v7.0.17
	github.com/rs/cors v1.7.0
	github.com/signintech/gopdf v0.9.8 // indirect
	github.com/stretchr/testify v1.5.1
	google.golang.org/api v0.28.0
	gopkg.in/tomb.v2 v2.0.0-20161208151619-d5d1b5820637 // indirect
	gopkg.in/yaml.v2 v2.3.0
)
